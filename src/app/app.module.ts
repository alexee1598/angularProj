import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app.routing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NotFoundComponent} from './not-found/not-found.component';
import {CoreModule} from './core/core.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatIconModule} from '@angular/material/icon';
import {NgxSpinnerModule} from 'ngx-spinner';
import {CookieService} from 'ngx-cookie-service';
import {CoreComponent, DialogOverviewCarDeleteComponent} from './core/core.component';

import {AuthServiceConfig, GoogleLoginProvider, FacebookLoginProvider} from 'angularx-social-login';
import {environment} from '../environments/environment';
import {OnlyNotAuthGuardService} from './shared/guards/not-auth.guard';
import {AuthComponent} from './authentification/auth/auth.component';
import {OnlyAuthGuardService} from './shared/guards/auth.guard';
import {CommonModule} from '@angular/common';
import {RegisterModule} from './authentification/register/register.module';
import {SentryErrorHandler} from './shared/services/error.service';

const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider(environment.GOOGLE_AUTH),
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider(environment.FACEBOOK_AUTH)
  }
]);

export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    CoreModule,
    NgbModule,
    RegisterModule,
    CommonModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatIconModule,
    NgxSpinnerModule
  ],
  entryComponents: [
    AuthComponent,
    CoreComponent,
    DialogOverviewCarDeleteComponent
  ],
  providers: [
    CookieService,
    { provide: ErrorHandler, useClass: SentryErrorHandler },
    OnlyAuthGuardService,
    OnlyNotAuthGuardService,
      {
        provide: AuthServiceConfig,
        useFactory: provideConfig,
      }
    ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {
}
