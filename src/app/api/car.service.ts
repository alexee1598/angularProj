import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Car} from '../shared/models/car';
import {CarsDetailed, GetCar, GetOwners, ResultResponse, Table} from '../shared/models/table';
import {CookieService} from 'ngx-cookie-service';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CarService {

  constructor(private http: HttpClient,
              private cookieService: CookieService,
              ) {
  }

  private baseUrl = environment.production ? environment.urlBackend : 'http://localhost:8000/';
  private baseCarUrl = `${this.baseUrl}cars`;
  httpHeaders = {
    headers: new HttpHeaders({
      Authorization: this.cookieService.get('Authorization').replace('%', ' ')
    })
  };

  getCars(): Observable<Table> {
    return this.http.get<Table>(`${this.baseCarUrl}/`, this.httpHeaders);
  }

  createCar(car: Car): Observable<ResultResponse> {
    return this.http.post<ResultResponse>(`${this.baseCarUrl}/view/car/`, car, this.httpHeaders);
  }

  deleteCar(id: number): Observable<ResultResponse> {
    return this.http.delete<ResultResponse>(`${this.baseCarUrl}/view/car/${id}/`, this.httpHeaders);
  }

  editCar(id: number): Observable<GetCar> {
    return this.http.get<GetCar>(`${this.baseCarUrl}/view/car/${id}/`, this.httpHeaders);
  }

  updateCar(car: Car): Observable<Car> {
    return this.http.put<Car>(`${this.baseCarUrl}/view/car/${car.id}/`, car, this.httpHeaders);
  }

  getCarsDetailed(): Observable<CarsDetailed> {
    return this.http.get<CarsDetailed>(`${this.baseCarUrl}/owners/`, this.httpHeaders);
  }

  getOwners(): Observable<GetOwners> {
    return this.http.get<GetOwners>(`${this.baseCarUrl}/view/car/`, this.httpHeaders);
  }

  getScraper(): Observable<any> {
    return this.http.get<any>(`${this.baseCarUrl}/scraper_cars/`, this.httpHeaders);
  }

}
