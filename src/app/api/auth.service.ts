import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {LoginResponse, UserResponse} from '../shared/models/user';
import {environment} from '../../environments/environment';

@Injectable()
export class ApiService {

  constructor(private http: HttpClient) {
  }

  private baseUrl = environment.production ? environment.urlBackend : 'http://localhost:8000/';

  public register(data: any): Observable<LoginResponse> {
    return this.http.post<LoginResponse>(`${this.baseUrl}account/register/`, data);
  }

  public sigIn(data: any): Observable<LoginResponse> {
    return this.http.post<LoginResponse>(`${this.baseUrl}account/login/`, data);
  }

  public loginService(data): Observable<UserResponse> {
    return this.http.post<UserResponse>(`${this.baseUrl}account/social-register/`, data);
  }

}
