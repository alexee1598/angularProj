import {Component, OnInit} from '@angular/core';
import {Owner} from '../../../shared/models/car';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CarService} from '../../../api/car.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-add-edit-owner',
  templateUrl: './owner-view.component.html',
  styleUrls: ['./owner-view.component.css']
})

export class OwnerViewComponent implements OnInit {
  owner = new Owner();
  form: FormGroup;
  ownerCreated: boolean;
  ownerEdited = false;
  edit = false;
  create = false;
  ownerId: number;

  constructor(private carService: CarService, private spinner: NgxSpinnerService, private route: ActivatedRoute) {

  }

  ngOnInit(): void {
    this.spinner.show();
    this.route.params
      .subscribe((idOwner) => {
          this.ownerId = idOwner.id;
          console.log(idOwner);
          console.log(idOwner.id);
        }, error => console.log(error)
      );
    this.spinner.hide();
    this.form = new FormGroup({
      inputName: new FormControl('', [
        Validators.required,
        Validators.pattern('\\S[0-9a-zA-Z ]{0,}'),
        Validators.maxLength(15),
        Validators.minLength(1)
      ]),
      inputSurname: new FormControl('', [
        Validators.required,
        Validators.pattern('\\S[0-9a-zA-Z ]{0,}'),
        Validators.maxLength(15),
        Validators.minLength(1)
      ]),
      inputAge: new FormControl('', [
        Validators.required,
        Validators.max(100),
        Validators.min(18)
      ]),
      inputEmail: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
    });
    if (this.ownerId !== undefined) {
      this.edit = true;
      this.reviewDataEdit();
    } else {
      this.create = true;
      this.reviewData();
    }
  }

  private reviewDataEdit() {

  }

  reviewData() {
    this.owner = new Owner();
  }


  onSubmit() {
    this.owner.name = this.form.controls.inputName.value;
    this.owner.surname = this.form.controls.inputSurname.value;
    this.owner.age = this.form.controls.inputAge.value;
    this.owner.email = this.form.controls.inputEmail.value;
    this.createOwner();
  }

  private createOwner() {

  }
}
