export class User {
  avatar: string;
  description: string;
  email: string;
  first_name: string;
  last_name: string;
  password: string;
  username: string;
}

export class LoginResponse {
  result: number;
  token: string;
  error: string;
}

export class UserResponse extends LoginResponse {
  user: User;
}
