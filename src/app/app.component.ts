import { Component } from '@angular/core';
import {CarService} from './api/car.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {AuthenticationService} from './api/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'untitled';


  constructor(private carService: CarService,
              private spinner: NgxSpinnerService,
              private auth: AuthenticationService) {
    // throw new Error('Sentry Test Error'); // TODO: remove
  }

  scraperCars() {
    this.spinner.show();
    this.carService.getScraper().subscribe(
      data => {
        if (data.result === 1) {
          this.spinner.hide();
        }
      }, error => {
        this.spinner.hide().then(() => {
        });
        alert('this option only authorized users');
      }
    );
  }

  logOut() {
    this.auth.logOut();
  }
}
