import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../api/auth.service';
import {CookieService} from 'ngx-cookie-service';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../api/authentication.service';
import {MustMatch} from "../../shared/validators/must-match.validators";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  mass = Array<number>();
  error = false;
  url: string | ArrayBuffer = '';
  avatar: File;

  constructor(private apiService: ApiService,
              private cookieService: CookieService,
              private formBuilder: FormBuilder,
              private router: Router,
              private auth: AuthenticationService) {
    for (let i = 18; i < 100; i++) {
      this.mass.push(i);
    }
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      age: ['', [Validators.required]],
      avatar: [''],
      username: ['', Validators.required],
      firstName: ['', [
        Validators.required,
        Validators.pattern('\\S[0-9a-zA-Z ]{0,}'),
        Validators.maxLength(40),
        Validators.minLength(2)
      ]],
      lastName: ['', [
        Validators.required,
        Validators.pattern('\\S[0-9a-zA-Z ]{0,}'),
        Validators.maxLength(40),
        Validators.minLength(2)
      ]],
      inputEmail: ['', [Validators.required, Validators.email]],
      password: ['', [
        Validators.required,
        Validators.minLength(6)]],
      confirmPassword: ['', Validators.required],
      confirmLicense: [false, Validators.required],
      description: ['']
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });
  }

  onSubmit() {
    this.submitted = true;

    if (this.form.invalid) {
      return;
    }

    const uploadData = new FormData();
    uploadData.append('avatar', this.avatar);
    uploadData.append('description', this.form.controls.description.value);
    uploadData.append('age', this.form.controls.age.value);
    uploadData.append('username', this.form.controls.username.value);
    uploadData.append('first_name', this.form.controls.firstName.value);
    uploadData.append('last_name', this.form.controls.lastName.value);
    uploadData.append('email', this.form.controls.inputEmail.value);
    uploadData.append('password', this.form.controls.password.value);
    this.registration(uploadData);
  }

  private registration(uploadData) {
    this.apiService.register(uploadData)
      .subscribe((data) => {
          if (data.result === 1) {
            this.auth.setCookies(data);
          } else {
            this.error = true;
          }
        },
        error => console.log(error))
    ;
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      this.avatar = event.target.files[0];
      reader.readAsDataURL(event.target.files[0]);

      reader.onload = (load) => {
        this.url = load.target.result;
      };
    }
  }
}
