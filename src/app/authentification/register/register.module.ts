import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RegisterComponent } from './register.component';
import { ApiService } from '../../api/auth.service';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AuthenticationService } from '../../api/authentication.service';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    RegisterComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    BrowserModule,
    RouterModule,
  ],
  providers: [
    ApiService,
    AuthenticationService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class RegisterModule {
}
