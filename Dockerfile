# Specifay a base image
FROM node:12.2.0 as builder

# Create some dir
WORKDIR '/app'

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

COPY package.json /app/package.json
RUN npm install
RUN npm install -g @angular/cli@9.1.0
COPY . /app
EXPOSE 4200
CMD ng serve --host 0.0.0.0
#RUN ng build --output-path=dist

#FROM nginx
#COPY --from=builder /app/dist /usr/share/nginx/html
